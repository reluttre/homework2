"""
@file elevator.py
@brief A finite state machine for an elevator
@details Simulates an elevators movement in response to button presses and sensor detections\n\n
Source code can be found at https://bitbucket.org/reluttre/homework2/src/master/

@author Robert Luttrell
@date 01/27/2021
"""
import random

def motor_cmd(cmd):
    """
    @brief              Simulates motor movement by printing "Mot <status>"
    """
    print("Mot " + cmd)

def simulateButtonPresses(buttonDict):
    """
    @brief              Simulates button presses
    @param buttonDict   Dictionary of <buttonName: str, buttonStatus: boolean> representing status of each button

    @details            Randomly presses any unpressed buttons in buttonDict
    """
    for buttonName in buttonDict:
        if not buttonDict[buttonName]:
            buttonDict[buttonName] = random.choice([True, False])

def detectFloorSensors(sensors, motorState):
    """
    @brief              Simulates floor sensor detection
    @param sensors      Dictionary of <floorName: str, sensorStatus: boolean> representing status of each floor sensor
    @param motorState   int representing state of motor

    @details            If the motor is moving in the direction of a given floor, randomly sets sensorStatus to True or False
    """
    # Moving down
    if motorState == 2:
        sensors["floor1"] = random.choice([True, False])
    # Moving up
    if motorState == 1:
        sensors["floor2"] = random.choice([True, False])

def clearFloorSensors(sensors):
    """
    @brief              Clears all floor sensors
    @param sensors      Dictionary of <floorName: str, sensorStatus: boolean> representing status of each floor sensor

    @details            Sets all sensors in sensors to false
    """
    for sensorName in sensors:
         sensors[sensorName] = False

if __name__ == "__main__":
    state = 0

    buttons = {"button1": False, "button2": False}
    sensors = {"floor1": False, "floor2": False}

    motorState = 2

    while True:
        simulateButtonPresses(buttons)
        detectFloorSensors(sensors, motorState)
        
        # Moving down
        if state == 0:
            print("S0")
            motor_cmd("down")
            motorState = 2

            if sensors["floor1"]:
                state = 1
                buttons["button1"] = False

        # Stopped at floor 1
        elif state == 1:
            print("S1")
            motor_cmd("stop")
            motorState = 0

            if buttons["button1"]:
                buttons["button1"] = False

            if buttons["button2"]:
                clearFloorSensors(sensors) # Floor sensors both False immediately following floor departure
                state = 2

        # Moving up
        elif state == 2:
            print("S2")
            motor_cmd("up")
            motorState = 1

            if sensors["floor2"]:
                state = 3
                buttons["button2"] = False

        # Stopped at floor 2
        elif state == 3:
            print("S3")
            motor_cmd("stop")
            motorState = 0

            if buttons["button2"]:
                buttons["button2"] = False

            if buttons["button1"]:
                clearFloorSensors(sensors) # Floor sensors both False immediately following floor departure
                state = 0

